# README #

### What is this repository for? ###

* Quickly display and make use of pagination data returned by Laravel's paginate functionality
* Version 1.0.3


### How do I get set up? ###

* Install the package into your project

```
#!javascript

bower install invento-paginate --save
```


* Include the following line at the bottom of your index.html:

```
#!html

<script src="bower_components/invento-response-message/dist/js/response-message.js"></script>
<script src="bower_components/invento-paginate/dist/js/paginate.js"></script>
```

* Include the following packages in your app's module:

```
#!javascript

'invento-response-message'
'invento-paginate'
```

* Add the following factory to your controller

```
#!javascript

Paginate
```


### Example ###

index.html

```
#!html

<html>
    <head>
        <script src="bower_components/invento-response-message/dist/js/response-message.js"></script>
        <script src="bower_components/invento-paginate/dist/js/paginate.js"></script>
    </head>
    <body>
        ...
    </body>
</html>
```

properties.js

```
#!javascript

var properties = angular.module('Properties', ['invento-response-message', 'invento-paginate']);

app.controller('properties', function($scope, Property, Paginate) {
    $scope.paginate = Paginate;
    $scope.paginate.request = Property.getProperties();
    $scope.paginate.getPage(1);
});
```
Note: The getPage method accepts an optional callback method which has the response object applied to it. You can access the object via the callback's this, e.g.
```
#!javascript
$scope.paginate.getPage(1, function() {
    console.log(this.response.data);
});
```
properties.html

```
#!html

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <table class="table table-nowrap table-striped">
                <thead>
                    <tr>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="property in paginate.items">
                        <td class="ellipsis">{{property.address_line_one}}</td>
                    </tr>
                </tbody>
            </table>

            <div class="col-xs-12 text-center" ng-if="paginate.items.length > 0 && paginate.pageArray[0].length > 1">
                <div class="pagination pagination-minimal">
                    <ul>
                        <li class="previous" ng-class="{disabled: paginate.currentPage == 1}"><a href="" ng-click="paginate.prevPage()"><span class="icon icon-left"></span></a></li>
                        <li class="more-results-before" ng-if="paginate.pageChunk > 0"><a href="" ng-click="paginate.prevChunk()"><span>&laquo;</span></a></li>
                        <li ng-repeat="n in paginate.pageArray[paginate.pageChunk]" ng-class="{active: n == paginate.currentPage}">
                            <a href="" ng-bind="n" ng-click="paginate.setPage(n)"></a>
                        </li>
                        <li class="more-results-after" ng-if="paginate.pageChunk == 0 && paginate.totalPages > 9"><a href="" ng-click="paginate.nextChunk()"><span>&raquo;</span></a></li>
                        <li class="next" ng-class="{disabled: paginate.currentPage == paginate.totalPages}"><a href="" ng-click="paginate.nextPage()"><span class="icon icon-right"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
```