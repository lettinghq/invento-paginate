(function() {
    'use strict';

    var paginate = angular.module('invento-paginate', ['invento-response-message']);

    paginate.factory('Paginate', function(ResponseMessage) {
        var Paginate = function() {};

        Paginate.prototype = {
            filters         : {},
            constantFilters : {},
            request         : false,
            stickyCallback  : false,
            items           : {},
            pageArray       : {},
            totalPages      : 1,
            totalChunks     : 1,
            currentPage     : 1,
            pageChunk       : 0,

            initialize: function() {
                this.filters = {};
            },

            setRequest : function(request) {
                this.request = request;
            },

            setStickyCallback : function(callback) {
                this.stickyCallback = callback;
            },

            getPage : function(page, callback) {
                var requestObject, self = this;

                if (typeof page !== 'number') {
                    page = 1;
                }

                this.items          = {};
                this.currentPage    = page;

                requestObject = $.extend({}, {page : page}, this.filters, this.constantFilters);

                this.request.get(requestObject, function(success) {
                    self.parseResponse.call(self, success, callback);
                }, function(error) {
                    ResponseMessage.error(error, true);
                });
            },

            parseResponse : function(success, callback) {
                var boundCallback, stickyCallback, totalItems, perPage, totalPages, pageArray, pos;

                totalItems  = success.response.total;
                perPage     = success.response.per_page;
                totalPages  = Math.ceil(totalItems / perPage);
                pageArray   = {};
                pos         = 0;

                for (var i = 1; i <= totalPages; i++) {
                    if (!pageArray[Math.floor(i / 10)]) {
                        pageArray[Math.floor(i / 10)] = [];
                    }

                    pageArray[Math.floor(i / 10)][pos] = i;

                    if (Math.floor((i + 1) / 10) != Math.floor(i / 10)) {
                        pos = 0;
                    } else {
                        pos++;
                    }
                }

                this.items        = success.response.data;
                this.totalPages   = totalPages;
                this.pageArray    = pageArray;
                this.totalChunks  = Object.keys(pageArray).length;

                if (typeof this.stickyCallback === 'function') {
                    stickyCallback = this.stickyCallback.bind(success);
                    stickyCallback();
                }

                if (typeof callback === 'function') {
                    boundCallback = callback.bind(success);
                    boundCallback();
                }
            },

            nextPage : function() {
                if (this.currentPage < this.totalPages) {
                    if ((this.pageChunk === 0 && this.currentPage === this.pageArray[this.pageChunk][8]) || (this.pageChunk > 0 && this.currentPage === this.pageArray[this.pageChunk][9])) {
                        this.pageChunk++;
                    }

                    this.currentPage++;
                    this.getPage(this.currentPage);
                }
            },

            nextChunk : function() {
                if (this.pageChunk < Object.keys(this.pageArray).length - 1) {

                    if (this.pageChunk > 0) {
                        this.currentPage = this.pageArray[this.pageChunk][9] + 1;
                    } else {
                        this.currentPage = this.pageArray[this.pageChunk][8] + 1;
                    }

                    this.pageChunk++;
                    this.getPage(this.currentPage);
                }
            },

            prevPage : function() {
                if (this.currentPage > 1) {
                    if (this.currentPage === this.pageArray[this.pageChunk][0]) {
                        this.pageChunk--;
                    }
                    
                    this.currentPage--;
                    this.getPage(this.currentPage);
                }       
            },

            prevChunk : function() {
                if (this.pageChunk > 0) {
                    this.currentPage = this.pageArray[this.pageChunk][0] - 1;
                    this.pageChunk--;
                    this.getPage(this.currentPage);
                }
            },

            setPage : function(page) {
                this.currentPage = page;
                this.getPage(page);
            },

            search : function() {
                this.currentPage = 1;
                this.pageChunk   = 0;

                this.getPage(1);
            },

            clear : function() {
                this.filters = {};

                this.search();
            },

            filtersSet : function() {
                return Object.keys(this.filters).length;
            }
        };

        return {
            create : function() {
                var paginate = new Paginate();
                paginate.initialize();
                return paginate;
            }
        };
    });

    paginate.directive('inventoPaginatePagers', function() {
        return {
            restrict: 'A',
            templateUrl: 'bower_components/invento-paginate/dist/html/pagers.html',
            replace: true,
        };
    });

})();
